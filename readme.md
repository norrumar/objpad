# ObjPad #

* Author: Joseph Lee, Cleverson Uliana and others
* Download [stable version][1]
* Download [development version][2]

This add-on provides quick commands to manage objects on screen, including navigation and other possibilities.

## Commands

* Control+NVDA+TAB: Steps through arrow key modes (see below for details).

## Arrow key modes

The add-on provides three ways to use arrow keys:

* Classic (or normal mode): use arrow keys to move cursor.
* Object nav: use arrow keys to move to next/previous/parent/first child objects.
* Web mode: use arrow keys to move between various elements.
* Scan mode: use arrow keys to move through objects on screen regardless of hierarchy.

The following commands are available with arrow keys set to object nav:

* Right arrow: next object.
* Left arrow: previous object.
* Up arrow: parent object.
* Down arrow: first child object.
* SPACE: activate.

With web mode active:

* Right arrow: next element.
* Left arrow: previous element.
* Up or down arrow: cycle through available web elements.

With scan mode active:

* Down arrow: next object.
* Up arrow: previous object.
* Right arrow: review next character.
* Left arrow: previous character.
* Control+right arrow: next word.
* Control+left arrow: previous word.
* SPACE: activate.

[1]: http://addons.nvda-project.org/files/get.php?file=objPad

[2]: http://addons.nvda-project.org/files/get.php?file=objPad-dev
